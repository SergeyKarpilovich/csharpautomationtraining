﻿using System;

namespace NET01._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dot a = new Dot(); //for testing indexer
            a[0] = 2.3;
            a[1] = 3.5;
            a[2] = 4.2;
            Console.WriteLine("Testing indexer with non-int indexes...  " + a.ToString());

            Dot b = new Dot(2.9, 4.7, 7.8); //dot for testing get and set properties
            b.Xcoord = -3.2;
            b.Ycoord = 13.46;
            b.Zcoord = 0.3;
            b.Mass = -0.1;
            Console.WriteLine($"Testing properties with negative mass: x = {b.Xcoord}, y = {b.Ycoord}, z = {b.Zcoord}, mass = {b.Mass}");

            b.Xcoord = 3.8;
            b.Ycoord = 123.06;
            b.Zcoord = -2.3;
            b.Mass = 0.1;
            Console.WriteLine($"Testing properties with positive mass: x = {b.Xcoord}, y = {b.Ycoord}, z = {b.Zcoord}, mass = {b.Mass}");

            Dot c = new Dot(1.0, 2.2, 12.9, 8.2); //just to prove that constructor with 4 args works properly
            Console.WriteLine($"Сonstructor with 4 args - object created with coordinates: x = {c.Xcoord}, y = {c.Ycoord}, z = {c.Zcoord}, mass = {c.Mass}");

            Dot any1 = new Dot(0.0, 0.0, 0.0); // 1st dot for testing IsZero method
            Dot any2 = new Dot(0.0, 0.1, 0.0); // 2nd dot for testing IsZero method
            Console.WriteLine("Testing IsZero Method - Negative scenario: " + any1.IsZero());
            Console.WriteLine("Testing IsZero Method - Positive scenario: " + any2.IsZero());

            //below testing the method for calculating distance between 2 dots
            Console.WriteLine("Testing - Distance between two dots, case1: {0}", Dot.Distance(new Dot(2.0, 1.2, 1.3), new Dot(4.5, 5.6, 6.7)));
            Console.WriteLine("Testing - Distance between two dots, case2: {0}", Dot.Distance(new Dot(0.0, 4.0, 0.0), new Dot(0.0, 2.0, 0.0)));
            Console.ReadKey();
        }
    }
}
