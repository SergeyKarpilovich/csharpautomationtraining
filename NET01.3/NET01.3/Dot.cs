﻿using System;

namespace NET01._3
{
    public class Dot
    {      
        private double _mass;

        public double Xcoord { get; set; } //autoproperty for X coordinate
        public double Ycoord { get; set; } //autoproperty for Y coordinate
        public double Zcoord { get; set; } //autoproperty for Z coordinate

        public double Mass
        {
            get { return _mass; }
            set { _mass = value >= 0 ? value : 0; }  // if mass < 0 then set it to 0
        }

        public double this[int i] //this indexer implementation with standard int indexes
        {
            get
            {
                switch (i)
                {
                    case 0:
                        return Xcoord;
                    case 1:
                        return Ycoord;
                    case 2:
                        return Zcoord;
                    default:
                        return 0;
                }
            }

            set
            {
                switch (i)
                {
                    case 0:
                        Xcoord = value;
                        break;
                    case 1:
                        Ycoord = value;
                        break;
                    case 2:
                        Zcoord = value;
                        break;
                    default:
                        break;
                }
            }
        }

        public double this[char i] //another indexer just to ensure index can be non-int
        {
            get
            {
                switch(i)
                {
                    case 'x':
                        return Xcoord;                        
                    case 'y':
                        return Ycoord;                        
                    case 'z':
                        return Zcoord;
                    default:
                        return 0;
                }
            }
            set
            {
                switch (i)
                {
                    case 'x':
                        Xcoord = value;
                        break;
                    case 'y':
                        Ycoord = value;
                        break;
                    case 'z':
                        Zcoord = value;
                        break;
                    default:
                        break;
                }
            }
        }

        public Dot() { }  //constructor without args - I use this for testing indexer implementation

        public Dot(double x, double y, double z)
        {
            Xcoord = x;
            Ycoord = y;
            Zcoord = z;
        }

        public Dot(double x, double y, double z, double mass) : this(x, y, z)
        {
            Mass = mass;
        }

        public bool IsZero() //method for prove that all dot coordinates are not 0
        => (Xcoord == 0 && Ycoord == 0 && Zcoord == 0);

        public static double Distance(Dot a, Dot b) //method for calculating the distance between 2 dots
        {
            return Math.Sqrt(Math.Pow(b.Xcoord - a.Xcoord, 2) + Math.Pow(b.Ycoord - a.Ycoord, 2) + Math.Pow(b.Zcoord - a.Zcoord, 2));
        }

        public override string ToString() //for indexer testing only
        => $"The coordinats of dot are: x = {this['x']}, y = {this['y']}, z = {this['z']}";
    }
}
