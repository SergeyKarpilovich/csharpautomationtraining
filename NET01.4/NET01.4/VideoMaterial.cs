﻿using System;

namespace NET01._4
{
    public class VideoMaterial: LinkMaterial //because video must have any link
    {
        public string PictureURI
        {
            get; set;
        }

        public VideoType VType
        {
            get; set;
        }

        public VideoMaterial (Guid id, string desc, string link, LinkType type, string picURI, VideoType vType): base(id, desc, link, LinkType.Video)
        {
            PictureURI = picURI;
            VType = vType;
        }

        public override string ToString()
        {
            return $"Material Id = {this.Id}    Material Description = {this.Description}   Link = {this.Link}  LinkType = {this.Type}  PictureURI = {this.PictureURI}  VideoType = {this.VType}";
        }

        public override bool Equals(object obj)
        {
            return !(obj is VideoMaterial compared) ? false : compared.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
