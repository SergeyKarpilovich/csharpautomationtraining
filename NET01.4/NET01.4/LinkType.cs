﻿namespace NET01._4
{
    public enum LinkType
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}
