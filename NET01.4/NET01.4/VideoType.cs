﻿namespace NET01._4
{
    public enum VideoType
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}
