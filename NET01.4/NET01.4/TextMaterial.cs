﻿using System;

namespace NET01._4
{
    public class TextMaterial: Material
    {
        private string text;

        public string Text
        {
          get { return text; }
          set { text = String.IsNullOrEmpty(value)? value : text; } //text should not be empty string or null
        }

        public TextMaterial(Guid id, string desc, string text): base(id, desc)
        {
            Text = text;
        }

        public override string ToString()
        {
            return $"Material Id = {this.Id}    Material Description = {this.Description}   Content = {this.Text}";
        }

        public override bool Equals(object obj)
        {
            return !(obj is TextMaterial compared) ? false : compared.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

    }
}
