﻿using System;
using System.Collections.Generic;

namespace NET01._4
{
    public class Lesson: Entity
    {        
        private List<Material> mat = new List<Material>(); // this list is for testing GetLessonKind() method

        public List<Material> AddMaterial(Material material) //method for add new material in list of materials for specific lesson
        {
            mat.Add(material);
            return mat;
        }

        public List<Material> AddMaterial(params Material[] mats) //overloaded method for add several materials for specific lesson
        {
            foreach (Material item in mats)
            {
                mat.Add(item);
            }
            return mat;
        }

        public Lesson (Guid id, string desc): base(id, desc)
        {
            
        }

        public override string ToString()
        {
            return $"Lesson Id: {this.Id}    Lesson Description: {this.Description}";
        }

        public override bool Equals(object obj)
        {
            return !(obj is Lesson compared) ? false : compared.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public LessonKind GetLessonKind(List<Material> li) //this method returns type of lesson (videolesson or textlesson)
        {
            if (li.Count != 0) {
                foreach (Material item in li)
                {
                    if (item is VideoMaterial)
                    {
                        return LessonKind.VideoLesson;
                    }
                }
                return LessonKind.TextLesson;
            }
            else throw new Exception("There is no materials in lesson!");            
        }
    }
}
