﻿using System;

namespace NET01._4
{
    public class Material: Entity  // this is base class for all materials (TextMaterial, LinkMaterial, VideoMaterial)
    {        

        public Material(Guid id, string desc): base(id, desc)
        {
            
        }

        public override bool Equals(object obj)  //override equals() for class Material and descendants
        {
            return !(obj is Material compared) ? false : compared.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
