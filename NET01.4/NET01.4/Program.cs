﻿using System;
using System.Collections.Generic;

namespace NET01._4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Lesson first = new Lesson(Guid.NewGuid(), "First Lesson");
            Lesson second = new Lesson(Guid.NewGuid(), "Second Lesson");
            Material my0 = new VideoMaterial(Guid.NewGuid(), "Videolecture 1", "http://#", LinkType.Video, "http://#", VideoType.Avi);
            Material my1 = new TextMaterial(Guid.NewGuid(), "Textlecture 1", "This is the first lecture");
            Material my2 = new LinkMaterial(Guid.NewGuid(), "Link 1", "LinkLecture 1", LinkType.Html);
            Material my3 = new LinkMaterial(Guid.NewGuid(), "Link 2", "LinkLecture 2", LinkType.Audio);
            Material my4 = new TextMaterial(Guid.NewGuid(), "Textlecture 2", "This is the second lecture");
                     
            var materialsForFirstLesson = first.AddMaterial(my0); // here I add video material my0 to the First Lesson
            var materialsForSecondLesson = second.AddMaterial(my2, my4, my3); //here I add several non-video materials to the Second Lesson
            Console.WriteLine($"Materials for {first.ToString()}\n     {materialsForFirstLesson.ToArray().GetValue(0)}\n");
            Console.WriteLine($"Materials for {second.ToString()}\n    {materialsForSecondLesson.ToArray().GetValue(0)}  {materialsForSecondLesson.ToArray().GetValue(1)}    {materialsForSecondLesson.ToArray().GetValue(2)}\n");

            Console.WriteLine("-----------------------------------------------");

            Console.WriteLine("First Lesson Kind is: " +  first.GetLessonKind(materialsForFirstLesson));            
            Console.WriteLine("Second Lesson Kind is: " + second.GetLessonKind(materialsForSecondLesson));


            //how equals() works
            Console.WriteLine($"Is material my0 equal to material my01?    {my0.Equals(my1)}");
            Console.WriteLine($"Is material my1 equal to material my01?    {my1.Equals(my1)}");

            // Code below is just FOR FUN to investigate how dictionary is implemented
            var lessonList = new Dictionary<Material, Lesson>(); 
            lessonList.Add(my0, first);
            lessonList.Add(my1, first);
            lessonList.Add(my2, second);
            lessonList.Add(my3, second);
            lessonList.Add(my4, second);
            //foreach (KeyValuePair<Material, Lesson> pair in lessonList)
            //{
            //    Console.WriteLine(pair.Key + "\t\t" + pair.Value);
            //}
            Console.ReadKey();
        }
    }
}