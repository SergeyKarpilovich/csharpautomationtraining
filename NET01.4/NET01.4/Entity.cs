﻿using System;

namespace NET01._4
{
    public class Entity
    {
        public const int MAX_DESC_LENGTH = 256;

        private string description;

        public Guid Id { get; set; }

        public string Description
        {
            get { return description; }
            set { description = value.Length < MAX_DESC_LENGTH ? value : description; }
        }

        public Entity(Guid id, string desc)
        {
            Id = id;
            description = desc;
        }

        public override bool Equals(object obj)
        {
            return !(obj is Entity compared) ? false : compared.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
