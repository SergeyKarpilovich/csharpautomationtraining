﻿using System;

namespace NET01._4
{
    public class LinkMaterial: Material
    {
        private string link;
        public string Link
        {
            get { return link; }
            set { link = String.IsNullOrEmpty(value)? value : link; }
        }

        public LinkType Type { get; set; }

        public LinkMaterial(Guid id, string desc, string link, LinkType linkType):base(id, desc)
        {
            Link = link;
            Type = linkType;
        }

        public override string ToString()
        {
            return $"Material Id = {this.Id}    Material Description = {this.Description}   Link = {this.Link}  LinkType = {this.Type}";
        }

        public override bool Equals(object obj)
        {
            return !(obj is LinkMaterial compared) ? false : compared.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
