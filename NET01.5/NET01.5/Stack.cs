﻿using System;

namespace NET01._5
{
    public class Stack<T> : IStack<T>
    {
        private T[] elements;
        private int count; //need for lines 27 and 34

        public int Count
        {
            get { return count; }
        }

        public bool IsEmpty => Count == 0;

        public Stack(int length)
        {
            elements = new T[length];
            count = 0;
        }

        public void Push(T item)
        {
            if (count == elements.Length)
                throw new InvalidOperationException("Stack overflow");
            elements[count++] = item;
        }

        public T Pop()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Stack is empty");
            T item = elements[--count];
            return item;
        }
    }
}