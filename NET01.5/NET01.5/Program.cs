﻿using System;

namespace NET01._5
{
    public class Program
    {
        //testing stack

        public static void Main(string[] args)
        {
            IStack<Complex> stack = new Stack<Complex>(5); //stack creation
            Complex[] complexArr = new Complex[5]; //complex structure creation

            complexArr[0].Re = 21.0;
            complexArr[0].Im = 2.2;
            complexArr[1].Re = 1.5;
            complexArr[1].Im = 2.7;
            complexArr[2].Re = 10.0;
            complexArr[2].Im = 6.2;
            complexArr[3].Re = 4.3;
            complexArr[3].Im = 9.12;
            complexArr[4].Re = 1.0;
            complexArr[4].Im = 6.6;

            for (int m = 0; m < 5; m++) //testing Stack.Push method with structure elements
            {
                stack.Push(complexArr[m]); 
            }
            Console.WriteLine("Pop the upper element:");
            Console.WriteLine(stack.Pop());             

            stack = stack.Reverse(); //testing Reverse extension method

            Console.WriteLine("\nUpper element after invoke Reverse method:");
            Console.WriteLine(stack.Pop());

            Console.WriteLine($"\nStack count: {stack.Count}");
            Console.WriteLine($"Stack is empty?: {stack.IsEmpty}");
            Console.Read();
        }
    }
}