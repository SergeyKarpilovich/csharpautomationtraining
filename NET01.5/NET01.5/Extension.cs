﻿namespace NET01._5
{
    public static class Extension //static class for extension method Reverse<T>()
    {
        public static IStack<T> Reverse<T>(this IStack<T> stack)
        {
            if (stack.IsEmpty)
                return stack;

            IStack<T> reversed = new Stack<T>(stack.Count);

            while (!stack.IsEmpty)
            {
                reversed.Push(stack.Pop());
            }
            return reversed;
        }
    }
}
