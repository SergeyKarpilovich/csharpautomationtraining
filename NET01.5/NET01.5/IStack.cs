﻿namespace NET01._5
{
    public interface IStack<T>
    {
        bool IsEmpty { get; }
        int Count { get; }

        T Pop();
        void Push(T element);
    }
}