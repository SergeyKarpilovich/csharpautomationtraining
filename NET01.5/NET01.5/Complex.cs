﻿namespace NET01._5
{
    public struct Complex
    {
        public double Re
        {
            get; set;
        }

        public double Im
        {
            get; set;
        }

        public Complex(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public override string ToString()
        {
            return $"Re = {Re}, Im = {Im}";
        }
    }
}
