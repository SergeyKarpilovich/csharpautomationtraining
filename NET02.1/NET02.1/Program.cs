﻿using System;

namespace NET02._1
{
    class Program
    {
        static void Main(string[] args)
        {
            SparseMatrix a = new SparseMatrix(4);
            a[0, 0] = 2;
            a[0, 0] = 6;
            a[0, 1] = 0;
            a[0, 2] = 25;
            a[1, 0] = 0;
            a[1, 1] = -13;
            a[1, 1] = -12;
            a[1, 1] = 0;
            a[2, 0] = 6;
            a[2, 1] = -13;
            a[2, 2] = 6;
            a[2, 3] = 15;
            a[3, 3] = -13;

            Console.WriteLine("All elements of Matrix:\n" + a);

            Console.WriteLine("All not zero elements in dictionary:");

            a.PrintDictionaryWithNotZeros();

            Console.WriteLine("\n" + new string('-', 20));
            Console.WriteLine("Sum of diagonal elements: " + a.Track());

            Console.WriteLine("\n" + new string('-', 20));
            Console.WriteLine("Count of 6 in matrix: " + a.CheckCount(6));
            Console.WriteLine("Count of 1 in matrix: " + a.CheckCount(1));
            Console.WriteLine("Count of -13 in matrix: " + a.CheckCount(-13));
            Console.WriteLine("Count of 0 in matrix: " + a.CheckCount(0));

            Console.Read();
        }
    }
}