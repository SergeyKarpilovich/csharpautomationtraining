﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NET02._1
{
    public class SparseMatrix : IEnumerable<int>
    {
        private readonly Dictionary<string, int> _notZeroElements;

        public int Size { get; }

        public int this[int i, int j]
        {
            get
            {
                if (i < 0 || i >= Size || j < 0 || j >= Size)
                    throw new Exception("You try to get an element out of Matrix boundary!");

                if (!_notZeroElements.ContainsKey($"[{i},{j}]"))
                    return 0;

                return _notZeroElements[$"[{i},{j}]"];
            }

            set
            {
                if (i < 0 || i >= Size || j < 0 || j >= Size)
                    throw new Exception("You try to set an element out of Matrix boundary!");

                if (value == 0)
                {
                    if (!_notZeroElements.ContainsKey($"[{i},{j}]"))
                        return;

                    _notZeroElements.Remove($"[{i},{j}]");
                        return;
                }

                if (!_notZeroElements.ContainsKey($"[{i},{j}]"))
                    _notZeroElements.Add($"[{i},{j}]", value);

                else
                    _notZeroElements[$"[{i},{j}]"] = value;
            }
        }

        public SparseMatrix(int size)
        {
            if (size <= 0) throw new Exception("Incorrect matrix size!");
            Size = size;
            _notZeroElements = new Dictionary<string, int>();
        }


        public void PrintDictionaryWithNotZeros() //for debug only
        {
            if (_notZeroElements.Count == 0)
                Console.WriteLine("All matrix elements are equal to 0");

            foreach (KeyValuePair<string, int> item in _notZeroElements)            
                Console.WriteLine(item.Key + " " + item.Value);
            
        }

        public IEnumerator<int> GetEnumerator()
        {
            for (int i = 0; i < Size;)
            {
                for (int j = 0; j < Size;)
                {
                    if (_notZeroElements.ContainsKey($"[{i},{j}]"))
                        yield return _notZeroElements[$"[{i},{j}]"];

                    else
                        yield return 0;

                    i++;
                    j++;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (_notZeroElements.ContainsKey($"[{i},{j}]"))
                        sb.Append(_notZeroElements[$"[{i},{j}]"]);
                    else
                        sb.Append("0");
                    sb.Append("\t");
                    if (j == Size - 1)
                        sb.Append("\n");
                }
            }

            return sb.ToString();
        }

        public int Track() // for summarize diagonal elements
        {
            int sum = 0;
            IEnumerator<int> it = GetEnumerator();

            while (it.MoveNext())
                sum += it.Current;

            return sum;
        }

        public int CheckCount(int x) //x frequency x in matrix
        {
            if (x == 0) //or (!_notZeroElements.ContainsValue(x)) but it is redundant and slow
                return Size * Size - _notZeroElements.Count;

            int query = _notZeroElements.Values.Count(y => y == x);

            return query;
        }
    }
}