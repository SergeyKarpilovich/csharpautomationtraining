﻿using System;
using System.Text;

namespace NET01._6
{
    public class Matrix<T>
    {
        private T[,] array;

        public int Size { get; }

        public event EventHandler<ChangeElementEventArgs<T>> Changed;

        protected virtual void OnChangeElement(object sender, ChangeElementEventArgs<T> e)
        {
            Changed?.Invoke(this, e);
        }

        public Matrix(int size)
        {
            if (size <= 0)
                VerifyMatrix<T>.ThrowEx("Incorrect matrix size!");
            Size = size;
            array = new T[Size, Size];
        }

        public T this[int i, int j]
        {
            get
            {
                if (i < 0 || i >= Size || j < 0 || j >= Size)
                    VerifyMatrix<T>.ThrowEx("You try to get an element out of Matrix boundary!");
                return array[i, j];
            }
            set
            {
                if (i < 0 || i >= Size || j < 0 || j >= Size)
                    VerifyMatrix<T>.ThrowEx("You try to set an element out of Matrix boundary!");
                T initialValue = array[i, j];
                array[i, j] = value;
                OnChangeElement(this, new ChangeElementEventArgs<T>(i, j, initialValue, value));
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    sb.Append(array[i, j]);
                    sb.Append("\t");
                    if (j == Size - 1)
                    {
                        sb.Append("\n");
                    }
                }
            }
            return sb.ToString();
        }
    }
}