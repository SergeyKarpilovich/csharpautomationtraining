﻿using System;

namespace NET01._6
{
    public class Program
    {
        public static void Main(string[] args)
        {           
            IntMatrix a = new IntMatrix(2);
            IntMatrix b = new IntMatrix(2);

            a.Changed += EventHandler;

            a.Changed += (sender, e) =>
            {
                Console.WriteLine($"Matrix a element Event handled by lambda-expression! Index: {e.Iindex},{e.Jindex} Old value: {e.OldValue} New value: {e.NewValue}\n");
            };

            b.Changed += delegate (object sender, ChangeElementEventArgs<int> e)
            {
                Console.WriteLine($"Matrix b element Event handled by anonymous method! Index: {e.Iindex},{e.Jindex} Old value: {e.OldValue} New value: {e.NewValue}\n");
            };

            a[0, 0] = 10;
            a[0, 1] = -18;
            a[1, 0] = -1;
            a[1, 1] = 5;

            b[0, 0] = 2;
            b[0, 1] = -3;
            b[1, 0] = 5;
            b[1, 1] = 4;

            Console.WriteLine("Print Matrix a:\n" + a);
            Console.WriteLine("Print Matrix b:\n" + b);
            Console.WriteLine("Print Matrix a + b:\n" + (a + b));
            Console.WriteLine("Print Matrix a * 2\n" + a * 2);
            Console.WriteLine("Print Matrix a * b\n" + a * b);
            Console.Read();
        }

        public static void EventHandler(object sender, ChangeElementEventArgs<int> e)
        {
            Console.WriteLine($"Event handled by method! Index: {e.Iindex},{e.Jindex} Old value: {e.OldValue} New value: {e.NewValue}\n");
        }
    }
}