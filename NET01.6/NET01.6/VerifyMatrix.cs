﻿namespace NET01._6
{
    public static class VerifyMatrix<T> //this class is for handle situations if matrix-parameters are null or unequal
    {
        private const string UNEQUAL_MATRIX_ERROR = "Matrix sizes are not equal!";
        private const string NULL_MATRIX_ERROR = "You try to operate with null-matrix!";

        internal static void ThrowEx(Matrix<T> a, Matrix<T> b) //for prevent double-coding
        {
            if (a is null || b is null) throw new MatrixException(NULL_MATRIX_ERROR);
            if (a.Size != b.Size) throw new MatrixException(UNEQUAL_MATRIX_ERROR);
        }

        internal static void ThrowEx(Matrix<T> a)
        {
            if (a is null) throw new MatrixException(NULL_MATRIX_ERROR);
        }

        internal static void ThrowEx(string mes)
        {
            throw new MatrixException(mes);
        }
    }
}