﻿using System;

namespace NET01._6
{
    public class MatrixException : Exception
    {
        public MatrixException()
        {
        }

        public MatrixException(string message) : base(message)
        {
        }

        public MatrixException(string message, Exception e) : base(message, e)
        {
        }
    }
}