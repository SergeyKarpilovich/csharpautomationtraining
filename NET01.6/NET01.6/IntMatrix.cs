﻿namespace NET01._6
{
    public class IntMatrix : Matrix<int>
    {
        public IntMatrix(int size) : base(size)
        {
        }

        public static IntMatrix operator +(IntMatrix a, IntMatrix b)
        {
            VerifyMatrix<int>.ThrowEx(a, b);
            IntMatrix c = new IntMatrix(a.Size);
            for (int i = 0; i < c.Size; i++)
            {
                for (int j = 0; j < c.Size; j++)
                {
                    c[i, j] = a[i, j] + b[i, j];
                }
            }
            return c;
        }

        public static IntMatrix operator *(IntMatrix a, int b)
        {
            VerifyMatrix<int>.ThrowEx(a);
            IntMatrix c = new IntMatrix(a.Size);
            for (int i = 0; i < c.Size; i++)
            {
                for (int j = 0; j < c.Size; j++)
                {
                    c[i, j] = a[i, j] * b;
                }
            }
            return c;
        }

        public static IntMatrix operator *(IntMatrix a, IntMatrix b)
        {
            VerifyMatrix<int>.ThrowEx(a, b);
            IntMatrix c = new IntMatrix(a.Size);
            for (int row = 0; row < a.Size; row ++)
            {
                for (int column = 0; column < b.Size; column++)
                {
                    for (int i = 0; i < c.Size; i ++)
                    {
                        c[row, column] += a[row, i] * b[i, column];
                    }
                }
            }
            return c;
        }
    }
}