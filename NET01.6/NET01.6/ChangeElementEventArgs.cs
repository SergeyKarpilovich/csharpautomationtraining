﻿using System;

namespace NET01._6
{
    public class ChangeElementEventArgs<T>: EventArgs
    {
        public T OldValue { get; }
        public T NewValue { get; }
        public int Iindex { get; }
        public int Jindex { get; }

        public ChangeElementEventArgs(int iindex, int jindex, T old, T @new)
        {
            Iindex = iindex;
            Jindex = jindex;
            OldValue = old;
            NewValue = @new;
        }
    }
}