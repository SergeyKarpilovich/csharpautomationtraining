﻿using System;

namespace NET01._2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter strings count: ");
            int length = int.Parse(Console.ReadLine()); //Count of elements in array
            string[] userStrings = new string[length];

            Console.WriteLine("Enter array elements: ");
            for (int it = 0; it < length; it++)
            {
                Console.WriteLine("Element" + it + " :");

                userStrings[it] = Console.ReadLine().ToLower();
            }

            Console.WriteLine("");

            Console.WriteLine("List of array elements: ");
            foreach (string item in userStrings) //Initial array of strings input
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("");

            int[] summa = new int[length];  //array for sum of vowels in every string

            char[][] tr = new char[length][]; //array of chars which every string consists of


            for (int a = 0; a < userStrings.Length; a++) //Dividing strings into char arrays
            {
                tr[a] = userStrings[a].ToCharArray();
            }

            for (int b = 0; b < userStrings.Length; b++) //Sum of vowels in every string
            {
                for (int h = 0; h < tr[b].Length; h++)
                {
                    if (tr[b][h] == 'a' || tr[b][h] == 'e' || tr[b][h] == 'i' ||
                        tr[b][h] == 'o' || tr[b][h] == 'u' || tr[b][h] == 'y')
                    {
                        summa[b] += 1;
                    }
                }
                Console.WriteLine("Sum of vowels in string " + b + ": " + summa[b] + "\n");
            }


            int temp;
            string tempStr;

            for (int p = 0; p < summa.Length; p++)  //String sorting by count of vowels (descending order)
            {
                for (int x = p + 1; x < summa.Length; x++)
                {
                    if (summa[x] > summa[p])
                    {
                        temp = summa[p];
                        summa[p] = summa[x];
                        summa[x] = temp;
                        tempStr = userStrings[p];
                        userStrings[p] = userStrings[x];
                        userStrings[x] = tempStr;
                    }
                }
            }

            Console.WriteLine("String sorting by count of vowels (descending order)\n------------------------------------");

            for (int i = 0; i < userStrings.Length; i++)
            {
                if (summa[i] != 0)
                {
                    Console.WriteLine(userStrings[i]);
                }
            }

            Console.ReadKey();
        }
    }
}
