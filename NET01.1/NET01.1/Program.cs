﻿using System;

namespace NET01._1
{
   public class Program
    {
       public static void Main(string[] args)
        {
            /* Console application summarize all integers falling into the user's range which can be whole divided by 3 or 5 but not by 20  */
            try
            {
                Console.Write("Please enter the minimal number of range (integer): ");
                int a = int.Parse(Console.ReadLine());
                Console.Write("Please enter the maximal number of range (integer): ");
                int b = int.Parse(Console.ReadLine());

                int sum = 0; // variable for сounting sum from the range
                for (int it = a; it <= b; it++)
                {

                    if ((it % 3 == 0 || it % 5 == 0) && it % 20 != 0)
                    {
                            sum += it;
                        //else continue;
                    }
                }
                Console.WriteLine("----------------------------------------------------------------------------------------");
                Console.WriteLine("Here is the sum of all integers from your range which can be whole divided by 3 or 5 but not by 20:\n\n" + sum);
                Console.WriteLine("----------------------------------------------------------------------------------------");
                Console.Write("Press any key to exit");
                Console.ReadKey();
            }
            catch (ArithmeticException exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}
